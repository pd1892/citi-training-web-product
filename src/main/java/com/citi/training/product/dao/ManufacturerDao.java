package com.citi.training.product.dao;

import java.util.List;
import com.citi.training.product.model.Manufacturer;

public interface ManufacturerDao {
	public List<Manufacturer> findAll();
	public Manufacturer findById(int id);
	public int create(Manufacturer manufacturer);
	public void deleteById(int id);
}
