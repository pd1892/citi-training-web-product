package com.citi.training.product.dao;

import java.util.List;

import com.citi.training.product.model.Product;

public interface ProductDao {
	public List<Product> findAll();
	public Product findById(int id);
	public int create(Product product);
	public void deleteById(int id);
}
