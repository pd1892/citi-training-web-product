package com.citi.training.product.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.ManufacturerDao;
import com.citi.training.product.model.Manufacturer;

@Component
public class MysqlManufacturerDao implements ManufacturerDao{
	@Autowired 
	JdbcTemplate tpl;
	
	public List<Manufacturer> findAll(){
		return tpl.query("SELECT id, name, price FROM product", new ManufacturerMapper());
	};

	@Override
	public Manufacturer findById(int id) {
        List<Manufacturer> product = this.tpl.query(
                "select id, name, address from manufacturer where id = ?",
                new Object[]{id},
                new ManufacturerMapper()
        );
        if(product.size() <= 0) {
            return null;
        }
        return product.get(0);
	};
	
	@Override
	public int create(Manufacturer manufacturer) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement("insert into manufacturer (name, address) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, manufacturer.getName());
                    ps.setString(2, manufacturer.getAddress());
                    return ps;
                }
            },
            keyHolder);
        return keyHolder.getKey().intValue();
	};
	
	public void deleteById(int id) {
			tpl.update("DELETE FROM manufacturer WHERE id = ?", id);			
	};

	private static final class ManufacturerMapper implements RowMapper<Manufacturer> {
		public Manufacturer mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Manufacturer(rs.getInt("id"),rs.getString("name"),rs.getString("address"));
		}
	}
}
