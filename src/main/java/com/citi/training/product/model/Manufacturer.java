package com.citi.training.product.model;

public class Manufacturer {
	private int id;
	private String name;
	private String address;
	
	public Manufacturer(int idInput, String nameInput, String addressInput) {
		id = idInput;
		name = nameInput;
		address = addressInput;
	}
	
	public int getId () {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	
	public void setId(int userInput) {
		id = userInput;
	}
	public void setName(String userInput) {
		name = userInput;
	}
	public void setAddress(String userInput) {
		address = userInput;
	}
} 
