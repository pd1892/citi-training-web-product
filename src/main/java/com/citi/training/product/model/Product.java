package com.citi.training.product.model;

public class Product {
	private int id;
	private String name;
	private double price;
	
	public Product(int inputID, String inputName, double inputPrice){
		id = inputID;
		name = inputName;
		price = inputPrice;
	}
	
	public void setId(int inputID) {
		id = inputID;
	}
	public void setName(String inputName) {
		name = inputName;
	}
	public void setPrice(double inputPrice) {
		price = inputPrice;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
}
