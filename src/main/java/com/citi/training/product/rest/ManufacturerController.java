package com.citi.training.product.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.product.model.Manufacturer;
import com.citi.training.product.service.ManufacturerService;

@RestController
@RequestMapping("/manufacturer")
public class ManufacturerController {
	@Autowired
	ManufacturerService manufacturerService;
	
	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Manufacturer> findAll() {
		return manufacturerService.findAll();
	}
	
	@RequestMapping(value="", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody Manufacturer manufacturer) {
		manufacturerService.create(manufacturer);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Manufacturer findById(@PathVariable int id) {
		return manufacturerService.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void delete(@PathVariable int id) {
		manufacturerService.deleteById(id);
	}
}
