package com.citi.training.product.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {
	@Autowired
	ProductService productService;
	
	@MockBean
	private ProductDao mockProductDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		when(mockProductDao.create(any(Product.class))).thenReturn(newId);
		int createId = productService.create(new Product(newId, "Ham", 9.99));
		
		verify(mockProductDao).create(any(Product.class));
		assertEquals(newId, createId);
	}
	
	@Test
	public void test_DeleteRuns() {
		int deleteTestId = 1;

		productService.deleteById(deleteTestId);
		
		verify(mockProductDao).deleteById(deleteTestId);
	}
	
	@Test
	public void test_FindIdRunes() {
		int findThisTestId = 1;
		String testName = "Cowhide";
		double testPrice = 99;
		Product testProduct = new Product(findThisTestId, testName, testPrice);
		when(mockProductDao.findById(findThisTestId)).thenReturn(testProduct);
		Product returnedProduct = productService.findById(findThisTestId);
		
		assertEquals(testProduct, returnedProduct);
		verify(mockProductDao).findById(findThisTestId);
	}
	
	@Test 
	public void test_FindAll() {
		List<Product> testList = new ArrayList<Product>();
		testList.add(new Product(0, "martina", 2));
		
		when(mockProductDao.findAll()).thenReturn(testList);
		productService.findAll();
		
		verify(mockProductDao).findAll();
	}
}
